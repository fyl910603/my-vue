const Koa = require('koa');
const app = new Koa();
//require('koa-router')返回的是函数
const router = require('koa-router')();

app.use(async (ctx, next) => {
    console.log(`Process ${ctx.request.method} ${ctx.request.url}...`);
    await next();
});
var homeListData = require('./home/list');
// router.get('/hello/:name', async (ctx,next) => {
//     let name = ctx.params.name;
//     ctx.response.body = `<h1>hello,${name}${homeListData.data} !</h1>`
//     //参数
// });

//推荐列表
router.get('/api/homelist', async (ctx,next) => {
    //参数
    ctx.response.body = homeListData;
});

//开始服务并生成路由
app.use(router.routes());
app.listen(3003);
console.log('app started at port 3000...');
