import Vue from 'vue'
import App from './App.vue'
import router from './router'
import 'babel-polyfill'
import fastclick from 'fastclick'
import './common/stylus/index.styl'
import ElementUI from 'element-ui';
import VueAwesomeSwiper from 'vue-awesome-swiper'

import 'element-ui/lib/theme-chalk/index.css';
import 'swiper/dist/css/swiper.css'

fastclick.attach(document.body);
Vue.config.productionTip = false

Vue.use(ElementUI,VueAwesomeSwiper)

new Vue({
  render: h => h(App),
  router
}).$mount('#app')
